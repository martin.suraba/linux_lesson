[root@localhost ~]# ls -l /etc/yum.repos.d/
total 48
-rw-r--r-- 1 root root 1664 Nov 23  2020 CentOS-Base.repo  --- zakladne repo
-rw-r--r-- 1 root root 1309 Nov 23  2020 CentOS-CR.repo
-rw-r--r-- 1 root root  649 Nov 23  2020 CentOS-Debuginfo.repo
-rw-r--r-- 1 root root  314 Nov 23  2020 CentOS-fasttrack.repo
-rw-r--r-- 1 root root  630 Nov 23  2020 CentOS-Media.repo
-rw-r--r-- 1 root root 1331 Nov 23  2020 CentOS-Sources.repo
-rw-r--r-- 1 root root 8515 Nov 23  2020 CentOS-Vault.repo
-rw-r--r-- 1 root root  616 Nov 23  2020 CentOS-x86_64-kernel.repo
-rw-r--r-- 1 root root 1358 Sep  4  2021 epel.repo ---> extra packages enterprise linux 
-rw-r--r-- 1 root root 1457 Sep  4  2021 epel-testing.repo
[root@localhost ~]#



konfiguracia yumu:

vi /etc/yum.conf

