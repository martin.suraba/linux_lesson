[root@92821d8f0c1c ~]# ls -lah .bash*
-rw-------. 1 root root 790 Jun  2 11:06 .bash_history
-rw-r--r--. 1 root root  18 Oct 15  2018 .bash_logout
-rw-r--r--. 1 root root 191 Jun  2 10:22 .bash_profile
-rw-r--r--. 1 root root 219 Jun  2 10:14 .bashrc
[root@92821d8f0c1c ~]# cat .bash_profile
# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH

yum update -y
[root@92821d8f0c1c ~]#
