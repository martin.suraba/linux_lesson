Physical volumes 

pvs -- ukaze zoznam

pvcreate

volume groups 

vgs -- ukaze zoznam

logical volumes 

lvs -- ukaze zoznam


uloha: -- potrebujeme vytvorit /data --- LVM 



 108  pvs
  109  vgs
  110  lvs
  111  pvcreate /dev/sdc
-- vytvorili sme /dev/sdc ako sucast Physical volumes

  112  pvs
  113  vgcreate datavg /dev/sdc
-- vytvorili sme volume group a pridali tam Physical volume /dev/sdc 

  114  pvs
  115  lvcreate -L 5G -n datalv datavg

  -- vytvorili sme logical volume, ktory ma 5 GB

  116  vgs
  117  ls /dev/mapper/datavg-datalv
  118  ls -l /dev/mapper/datavg-datalv
  119  mkfs.xfs /dev/mapper/datavg-datalv

-- naformatovali sme logical volume, ktory ma file system xfs 

  120  vi /etc/fstab

-- 
/dev/mapper/datavg-datalv /data  xfs       defaults        0 0


  121  mount -a
  122  mkdir /data

  -- vytvorili sme adresar /data a potom mountli
  123  mount -a
  124  df -hT
