CHANGE : zakaznik potrebuje disk /data2, ktory bude mat 1GB 

1. mkdir /data2

2. vytvorime physical volume

3. vytvorime volume group 

4. vytvorime logical volume

5. vytvorime file system na logical volume

6. mountneme to pomocou /etc/fstab

prikazy: 

1. mkdir /data2

2. pvcreate /dev/sdc

3. vgcreate datavg /dev/sdc

4. lvcreate -L 1G datalv datavg 

5. mkfs.xfs /dev/mapper/datavg-datalv

6. vi /etc/fstab 

bud: 

/dev/mapper/datavg-datalv /data2 xfs defaults 0 0 

(najdeme si UUID a to pripojime)

UUID="xyz" /data2 xfs defaults 0 0 

po odchode z /etc/fstab

mount -av 

df -hT /data2