nainstalujme si web server (httpd)

- povolme port 80 v firewalld

1. yum install httpd -y 

systemctl status httpd 

- pokial je vypnuta

systemctl start httpd

systemctl enable httpd

2. povolime port 80 


firewall-cmd --add-port=80/tcp

firewall-cmd --permanent --add-port=80/tcp

# ked pridate nejake pravidlo, tak potrebujete restartovat firewall pravidla
firewall-cmd --reload

#ak potrebujete check: 
firewall-cmd --list-ports





