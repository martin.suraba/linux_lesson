[root@rambo ~]# cd /training/
[root@rambo training]# cp /etc/yum.conf /training
[root@rambo training]# pwd
/training
[root@rambo training]# ls
yum.conf
[root@rambo training]# ls /training/
yum.conf
[root@rambo training]# cp yum.conf yum.conf.bak
[root@rambo training]# ls -l
total 8
-rw-r--r--. 1 root root 1028 Jun 22 12:39 yum.conf
-rw-r--r--. 1 root root 1028 Jun 22 12:41 yum.conf.bak
[root@rambo training]# mv yum.conf.bak naucil_som_sa_backupy_yum_conf
[root@rambo training]# ls -l
total 8
-rw-r--r--. 1 root root 1028 Jun 22 12:41 naucil_som_sa_backupy_yum_conf
-rw-r--r--. 1 root root 1028 Jun 22 12:39 yum.conf
[root@rambo training]# rm yum.conf
rm: remove regular file ‘yum.conf’? y
[root@rambo training]# touch subor.txt
[root@rambo training]# ls -l
total 4
-rw-r--r--. 1 root root 1028 Jun 22 12:41 naucil_som_sa_backupy_yum_conf
-rw-r--r--. 1 root root    0 Jun 22 12:46 subor.txt
[root@rambo training]#
