filtrovanie logov:

# directory of syslog 
1. cd /etc/rsyslog.d/
 
2. ll 

# create config file
3. touch docker.conf 


# only 1 if (if $programname == 'the name of service' then PATH to saving logs)
4. vi docker.conf 

if $programname == 'dockerd' then {

    	/var/log/docker.log
    	~

}

# restart rsyslog 

5. systemctl restart rsyslog
