zakaznik chce /app  - a chce tam lvm 

1. vytovrime adresar /app 

2. pridame disk /dev/sdc --> physical volumes

3. vytvorime volume group --> /dev/sdc ---> volume group app_vg

4. vytvorime logical volume ----> z volume gropu app_vg --> logical volume app_lv

5. vytvorime si file system 

6. mount 

7. df -hT 

1. mkdir /app

2. [root@computer1 ~]# pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[root@computer1 ~]# pvs
  PV         VG     Fmt  Attr PSize   PFree
  /dev/sda2  centos lvm2 a--  <79.00g 4.00m
  /dev/sdc          lvm2 ---    2.00g 2.00g
3. [root@computer1 ~]# vgcreate app_vg /dev/sdc
  Volume group "app_vg" successfully created
[root@computer1 ~]# pvs
  PV         VG     Fmt  Attr PSize   PFree
  /dev/sda2  centos lvm2 a--  <79.00g  4.00m
  /dev/sdc   app_vg lvm2 a--   <2.00g <2.00g
[root@computer1 ~]# vgs
  VG     #PV #LV #SN Attr   VSize   VFree
  app_vg   1   0   0 wz--n-  <2.00g <2.00g
  centos   1   3   0 wz--n- <79.00g  4.00m
[root@computer1 ~]# lvs
  LV   VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home centos -wi-ao---- <25.12g
  root centos -wi-ao----  50.00g
  swap centos -wi-ao----  <3.88g
[root@computer1 ~]#


4. [root@computer1 ~]# lvcreate -L 1G -n app_lv app_vg
  Logical volume "app_lv" created.
[root@computer1 ~]# vgs
  VG     #PV #LV #SN Attr   VSize   VFree
  app_vg   1   1   0 wz--n-  <2.00g 1020.00m
  centos   1   3   0 wz--n- <79.00g    4.00m
[root@computer1 ~]# lvs
  LV     VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  app_lv app_vg -wi-a-----   1.00g
  home   centos -wi-ao---- <25.12g
  root   centos -wi-ao----  50.00g
  swap   centos -wi-ao----  <3.88g
[root@computer1 ~]#


5. [root@computer1 ~]# mkfs.xfs /dev/mapper/app_vg-app_lv 
6. [root@computer1 ~]# vi /etc/fstab

/dev/mapper/app_vg-app_lv /app                  xfs     defaults        0 0



[root@computer1 ~]# mount -a
7. [root@computer1 ~]# df -hT /app
Filesystem                Type  Size  Used Avail Use% Mounted on
/dev/mapper/app_vg-app_lv xfs  1014M   33M  982M   4% /app
[root@computer1 ~]#

