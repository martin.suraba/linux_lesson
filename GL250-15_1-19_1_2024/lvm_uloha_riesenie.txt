uloha: 

/data1 

- > vytvorme lvm 

...

0. mkdir /data1
 
1. pvcreate /dev/sdc

2. vgcreate data_vg /dev/sdc 

3.  lvcreate -n data_lv -L5.01G data_vg

4. mkfs.xfs /dev/mapper/data_vg-data_lv

5. vi /etc/fstab 

/dev/mapper/data_vg-data_lv /data1 xfs defaults 0 0 

6. mount -a 
