#nfs -server 

yum install nfs-utils -y
vi /etc/exports
--> insert to file: 
/nfs *(rw)

mkdir /nfs
chmod 777 /nfs
exportfs -a
systemctl start {rpcbind,nfs-server,rpc-statd,nfs-idmapd}
showmount -e localhost
ip a s

systemctl stop firewalld 
