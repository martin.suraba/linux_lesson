prikazy: 

    1  ip a --> show ip addresses: https://access.redhat.com/sites/default/files/attachments/rh_ip_command_cheatsheet_1214_jcs_print.pdf 
    2  uptime - show how long is PC up
    3  uptime -s
    4  df -hT
    5  crontab -l
    6  ip a s
    7  ip r s -- show routing https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-networkscripts-static-routes 
    8  ip route show
    9  crontab -e - edit crontabs
   10  crontab -l
   11  crontab -e
   12  pwd
   13  vi run.sh  -- writing scripts: https://gitlab.com/martin.suraba/linux_lesson/-/blob/main/2024/linux%20lesson_14_6_2024/basic_script.sh 
   14  timedatectl set-timezone "Europe/Prague"  -set timestamp: https://www.thegeekdiary.com/timedatectl-command-examples-in-linux/ 
   15  date
   16  ls -l /etc/localtime
   17  crontab -l
   18  chmod +x run.sh
   19  reboot
   20  ps -ef | grep -i .sh
   21  ls
   22  crontab -l
   23  ps -ef | grep -i .sh
   24  crontab -e ---> edit  crontabs
   25  which sh
   26  crontab -e
   27  ps -ef | grep -i .sh - processes 
   28  reboot
   29  ps -ef | grep -i .sh
   30  top -p 916
   31  top
   32  top -p 916
   33  mc
   34  ps -ef | grep -i yum
   35  tail -f /var/log/dnf.log   -- checking in log files process of update
   36  cat /var/log/dnf.log
   37  ps -ef | grep -i yum
   38  history

Mistake:

my human mistake 

(chyba so zapisom cronu) --> moja syntakticka chyba


bad

 @reboot ---> sh run.sh

good: 

crontab -e 

@reboot /usr/bin/sh /root/run.sh
