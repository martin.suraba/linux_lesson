#!/bin/bash

yum install mc wget zip unzip -y

timedatectl set-timezone "Europe/Prague"

yum update -y

crontab -r
